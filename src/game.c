#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "game.h"
#include "constants.h"

// --------------------------- //
// Private function prototypes //
// --------------------------- //
bool Game_validMove(struct Game *game, enum Direction direction);

struct Game *Game_initialize(SDL_Renderer *renderer)
{
	struct Game *game;
	game = (struct Game*)malloc(sizeof(struct Game));
	game->renderer = renderer;
	game->map = Map_create(MAP_FILENAME, renderer);
	game->character = Character_create(renderer);
	game->donut_1 = Donut_create(renderer);
	game->donut_2 = Donut_create(renderer);
	game->donut_3 = Donut_create(renderer);
	Donut_location_assigner(DONUT_LOCATION_X_1, DONUT_LOCATION_Y_1, game->donut_1);
	Donut_location_assigner(DONUT_LOCATION_X_2, DONUT_LOCATION_Y_2, game->donut_2);
	Donut_location_assigner(DONUT_LOCATION_X_3, DONUT_LOCATION_Y_3, game->donut_3);
	game->donutLeft = 3;
	game->state = GAME_PLAY;
	return game;
}

void Game_delete(struct Game *game)
{
	if (game != NULL) {
		Map_delete(game->map);
		Character_delete(game->character);
		Donut_delete(game->donut_1);
		Donut_delete(game->donut_2);
		Donut_delete(game->donut_3);
		free(game);
	}
}

void Game_run(struct Game *game)
{
	SDL_Event e;
	game->gameStartTime = SDL_GetTicks();
	while (game->state == GAME_PLAY) {
		if(game->character->yVelocity < 20)
			game->character->yVelocity += GRAVITY;
		Character_move(game->character, MOVE_DURATION);
		Game_checkEdgeCollision(game);
		Game_checkWallCollision(game);
		while (SDL_PollEvent(&e) != 0) {
			if (e.type == SDL_QUIT || (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE)) {
				game->state = GAME_QUIT;
			} else {
				Character_input(game->character, e);
			}
		}
		SDL_SetRenderDrawColor(game->renderer, 0x00, 0x00, 0x00, 0x00);
		SDL_RenderClear(game->renderer);
		Map_render(game->map);
		Character_render(game->character);
		//Check collision with donuts
		int startX = min(game->character->currentMove.target.x, game->character->screenPosition.x);
		int width = abs(game->character->currentMove.target.x - game->character->screenPosition.x) + CHARACTER_WIDTH;
		int startY = min(game->character->currentMove.target.y, game->character->screenPosition.y);
		int height = abs(game->character->currentMove.target.y - game->character->screenPosition.y) + CHARACTER_HEIGHT;
		SDL_Rect hitboxChar = {
			startX,
			startY,
			width,
			height};
		if(!game->donut_1->found && SDL_HasIntersection(&game->donut_1->hitbox, &hitboxChar)) {
			game->donut_1->found = true;
			game->donutLeft -= 1;
		}
		if(!game->donut_2->found && SDL_HasIntersection(&game->donut_2->hitbox, &hitboxChar)) {
			game->donut_2->found = true;
			game->donutLeft -= 1;
		}
		if(!game->donut_3->found && SDL_HasIntersection(&game->donut_3->hitbox, &hitboxChar)) {
			game->donut_3->found = true;
			game->donutLeft -= 1;
		}
		if (!game->donut_1->found)
			Donut_render(game->donut_1);
		if (!game->donut_2->found)
			Donut_render(game->donut_2);
		if (!game->donut_3->found)
			Donut_render(game->donut_3);
		if (game->donutLeft == 0)
			game->state = GAME_WIN;
		if (SDL_GetTicks() - game->gameStartTime > TIME_LIMIT_MINUTES * 60 * 1000)
			game->state = GAME_LOSE;
		Game_RenderTimer(game);
		SDL_RenderPresent(game->renderer);
	}
}

void Game_checkEdgeCollision(struct Game *game)
{
	//Can't go outside of window, so the caracter will move right up to the edge if the move would take him out of the window.
	if (game->character->currentMove.target.x < 0 + MAP_WALL_WIDTH)
	{
		game->character->currentMove.target.x = 0;
	}
	else if (game->character->currentMove.target.x + CHARACTER_WIDTH > SCREEN_WIDTH - MAP_WALL_WIDTH)
	{
		game->character->currentMove.target.x = SCREEN_WIDTH - CHARACTER_WIDTH - MAP_WALL_WIDTH * 1.5;
	}
	if (game->character->currentMove.target.y < 0 + MAP_WALL_WIDTH)
	{
		game->character->currentMove.target.y = 0 + MAP_WALL_WIDTH / 2;
	}
	else if (game->character->currentMove.target.y + CHARACTER_HEIGHT >= SCREEN_HEIGHT - MAP_WALL_WIDTH)
	{
		game->character->currentMove.target.y = SCREEN_HEIGHT - CHARACTER_HEIGHT - MAP_WALL_WIDTH * 1.5;
	}
}

void Game_checkWallCollision(struct Game *game)
{

	SDL_Rect hitboxChar2 = {
		game->character->currentMove.target.x,
		game->character->currentMove.target.y,
		35,
		75};

	SDL_Rect hitboxChar = {
		game->character->screenPosition.x,
		game->character->screenPosition.y,
		35,
		75};
	for (int i = 0; i < sizeof(game->map->coordPoints) / sizeof(SDL_Rect); ++i)
	{
		if (SDL_HasIntersection(&hitboxChar, &game->map->coordPoints[i]))
		{
			if (game->character->screenPosition.y <= game->map->coordPoints[i].y)
			{
				if (game->character->yVelocity > 0)
				{
					game->character->currentMove.target.y = game->character->screenPosition.y;
					game->character->currentMove.source.y = game->character->screenPosition.y;
					game->character->screenPosition.y = game->character->screenPosition.y - 30;
					game->character->yVelocity = 0;
				}
			}
			if (game->character->screenPosition.y + game->map->coordPoints[i].h >= game->map->coordPoints[i].y)
			{
				game->character->currentMove.target.y = game->character->screenPosition.y + 10;
				game->character->currentMove.source.y = game->character->screenPosition.y + 10;
				game->character->screenPosition.y = game->character->screenPosition.y + 10;
				game->character->yVelocity = 0;
			}
		}
		if (SDL_HasIntersection(&hitboxChar2, &game->map->coordPoints[i])){
			if (game->character->currentMove.source.x + 35 <= game->map->coordPoints[i].x)
			{
				game->character->currentMove.target.x = game->character->currentMove.source.x;
				game->character->xVelocity = 0;
			}

			if (game->character->currentMove.source.x >= game->map->coordPoints[i].x + game->map->coordPoints[i].w)
			{
				game->character->currentMove.target.x = game->character->currentMove.source.x;
				game->character->xVelocity = 0;
			}
		}
	}
}

void Game_RenderTimer(struct Game *game)
{
	TTF_Font* Sans = TTF_OpenFont("../assets/sans.ttf", 80);

	SDL_Color Black = {0, 0, 0};
	int timePassed = SDL_GetTicks() - game->gameStartTime;
	int minutes = TIME_LIMIT_MINUTES - timePassed / 1000 / 60 -1;
	int seconds = 60 - (timePassed / 1000 % 60);
	char *timerString = malloc(sizeof(char) * 8);
	sprintf(timerString, "%d:%02d", minutes, seconds);
	SDL_Surface* surfaceMessage = TTF_RenderText_Solid(Sans, timerString, Black);

	SDL_Texture* Message = SDL_CreateTextureFromSurface(game->renderer, surfaceMessage);

	SDL_Rect Message_rect; //create a rect
	Message_rect.x = 20;  //controls the rect's x coordinate
	Message_rect.y = 10; // controls the rect's y coordinte
	Message_rect.w = 290; // controls the width of the rect
	Message_rect.h = 100; // controls the height of the rect

	SDL_RenderCopy(game->renderer, Message, NULL, &Message_rect);
	TTF_CloseFont(Sans);

	free(surfaceMessage);
}
